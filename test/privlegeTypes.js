const privilegeTypes = {}

privilegeTypes.CREATE_GROUP = { name: 'createGroup' }
privilegeTypes.REMOVE_GROUP = { name: 'removeGroup', params: { groups: [ String ] } }

privilegeTypes.VIEW_SCRIPTS = { name: 'viewScripts', params: { groups: [ String ] } }
privilegeTypes.VIEW_RUNS = { name: 'viewRuns', params: { groups: [ String ] } }
privilegeTypes.VIEW_USERS = { name: 'viewUsers', params: { groups: [ String ] } }

privilegeTypes.EDIT_GROUP = { name: 'editGroup', params: { groups: [ String ] } }
privilegeTypes.ADD_USER_TO_GROUP = { name: 'addUserToGroup', params: { groups: [ String ] } }
privilegeTypes.INTIVE_USER = { name: 'inviteUser', params: { groups: [ String ] } }
privilegeTypes.REMOVE_USER_FROM_GROUP = { name: 'removeUserFromGroup', params: { groups: [ String ] } }
privilegeTypes.CHANGE_USER_GROUP_ROLE = { name: 'changeUserGroupRole', params: { groups: [ String ] } }

privilegeTypes.CREATE_SCRIPT = { name: 'createScript', params: { groups: [ String ] } }
privilegeTypes.DELETE_SCRIPT = { name: 'deleteScript', params: { groups: [ String ] } }
privilegeTypes.EDIT_SCRIPT = { name: 'editScript', params: { groups: [ String ] } }
privilegeTypes.RUN_SCRIPT = { name: 'runScript', params: { groups: [ String ] } }
privilegeTypes.CANCEL_SCRIPT = { name: 'cancelScript', params: { groups: [ String ] } }

// privilegeTypes.SITE_OWNER = { name: 'siteOwner' }

module.exports = privilegeTypes
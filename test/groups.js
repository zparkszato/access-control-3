const defaultRoles = [
  {
    "name": "Owner",
    "privileges": [
      {
        "name": "removeGroup",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "viewScripts",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "viewRuns",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "viewUsers",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "editGroup",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "addUserToGroup",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "inviteUser",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "removeUserFromGroup",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "changeUserGroupRole",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "createScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "deleteScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "editScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "runScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "cancelScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      }
    ]
  },
  {
    "name": "Admin",
    "privileges": [
      {
        "name": "viewScripts",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "viewRuns",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "viewUsers",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "addUserToGroup",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "inviteUser",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "removeUserFromGroup",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "createScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "deleteScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "editScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "runScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "cancelScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      }
    ]
  },
  {
    "name": "User",
    "privileges": [
      {
        "name": "viewScripts",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "viewRuns",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "viewUsers",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "runScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "cancelScript",
        "params": {
          "groups": [
            "self"
          ]
        }
      }
    ]
  },
  {
    "name": "Readonly",
    "privileges": [
      {
        "name": "viewScripts",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "viewRuns",
        "params": {
          "groups": [
            "self"
          ]
        }
      },
      {
        "name": "viewUsers",
        "params": {
          "groups": [
            "self"
          ]
        }
      }
    ]
  }
]

module.exports = [
  {
    "_id": "12345",
    "hosts": [ ],
    "name": "Test Group",
    "members": [
      {
        id: "5d8236771dc2b9c27e8d8ec9",
        role: "User"
      }
    ],
    "roles": [ ...defaultRoles ],
    "privileges": [
      {
        "name": "viewRuns",
        "params": {
          "groups": [
            "any"
          ]
        }
      },
    ],
    "defaultRole": "User",
  },
  {
    "_id": "67890",
    "hosts": [ ],
    "name": "Admin",
    "members": [
      // {
      //   id: "5d8236771dc2b9c27e8d8ec9",
      //   role: "User"
      // }
    ],
    "roles": [ ...defaultRoles ],
    "privileges": [
      {
        "name": "createGroup",
        "params": null
      },
      {
        "name": "removeGroup",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "viewScripts",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "viewRuns",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "viewUsers",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "editGroup",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "addUserToGroup",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "inviteUser",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "removeUserFromGroup",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "changeUserGroupRole",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "createScript",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "deleteScript",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "editScript",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "runScript",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      },
      {
        "name": "cancelScript",
        "params": {
          "groups": [
            "any",
            "not-self"
          ]
        }
      }
    ],
    "defaultRole": "User",
  }
]
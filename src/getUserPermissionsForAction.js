const getUserPermissions = require('./getUserPermissions')

const getUserPermissionsForAction = (action, user, groups) => (
  getUserPermissions(user, groups).find(e => e.name === action)
)

module.exports = getUserPermissionsForAction
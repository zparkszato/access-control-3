const canDoWithPermissions = require('./canDoWithPermissions')
const getUserPermissions = require('./getUserPermissions')

const canUserDo = (action, params, user, groups) => {
  const perms = getUserPermissions(user, groups)
  return canDoWithPermissions(action, params, perms)
}

module.exports = canUserDo
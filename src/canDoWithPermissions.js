const { builtIns } = require('./utils')

const validators = builtIns.paramValidators

const canDoWithPermissions = (action, params, permissions) => {
  const perm = permissions.find(e => e.name === action)
  if (!perm) return false
  
  return (
    Object.entries(params)
    .map(([ key, val ]) => {
      if (!validators[key]) {
        console.warn(`Provided a param ("${key}") for which there is no validator`)
        return true
      }
      return validators[key](perm.params, val)
    })
    .every(e => e)
  )
}

module.exports = canDoWithPermissions
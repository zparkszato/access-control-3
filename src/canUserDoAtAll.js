const permissionExistsForAction = require('./permissionExistsForAction')
const getUserPermissions = require('./getUserPermissions')

const canUserDoAtAll = (action, user, groups) => {
  const perms = getUserPermissions(user, groups)
  return permissionExistsForAction(action, perms)
}

module.exports = canUserDoAtAll
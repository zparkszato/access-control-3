const utils = require('./utils')

const paramMap = {
  groups: (param, group) => (
    param.map(p => {
      if (p === 'self') return group._id.toString()
      if (p === 'not-self') return `!${group._id}`
      if (p === 'any') return '*'
      return p.toString()
    })
  )
}

const paramValidators = {
  group: ({ groups }, value) => {
    if (groups.includes(utils.excludeForm(value))) return false
    if (groups.includes('*')) return true
    if (groups.find(e => utils.isExclude(e))) return true
    if (groups.includes(value)) return true
    return false
  }
}

module.exports = {
  paramMap,
  paramValidators,
}
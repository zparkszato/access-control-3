const includeForm = str => str.toString().replace(/!/, '')
const excludeForm = str => `!${includeForm(str)}`
const isExclude = str => str.toString().match(/^!.*?$/) ? true : false
const isInclude = str => !isExclude(str)

module.exports = {
  includeForm,
  excludeForm,
  isExclude,
  isInclude,
}
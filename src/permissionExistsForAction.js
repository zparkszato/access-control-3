const permissionExistsForAction = (action, permissions) => {
  const perm = permissions.find(e => e.name === action)
  if (perm) return true
  return false
}

module.exports = permissionExistsForAction
const merge = require('deepmerge')

const {
  includeForm,
  excludeForm,
  isExclude,
  isInclude,
  builtIns,
} = require('./utils')
  

const paramMap = builtIns.paramMap

const groupPermissions = permissions => (
  permissions
  .reduce((acc, perm) => ({
    ...acc,
    [perm.name]: [
      ...(acc[perm.name] || []),
      {
        params: perm.params || {}
      }
    ]
  }), {})
)

const combineMerge = (target, source, options) => {
  let destination = target.slice()
  for (let item of source) {
    if (!(isExclude(item) && destination.includes(includeForm(item)))) {
      if (isInclude(item) && destination.includes(excludeForm(item))) destination = destination.filter(e => e !== excludeForm(item))
      if (!destination.includes(item)) destination.push(item)
    }
  }
  return destination
}

const mergePermissions = permissions => {
  return Object.entries(groupPermissions(permissions))
  .map(([name, list]) => ({
    name,
    ...merge.all(
      list,
      {
        arrayMerge: combineMerge,
      }
    )
  }))
}

const getUserPermissions = (user, groups=[]) => {
  let userPrivileges = []
  for (let group of groups) {

    const member = group.members.find(m => m.id.toString() === user._id.toString())
    if (member) {
      const role = member ? member.role : null
      const rolePrivileges = role ? group.roles.find(r => r.name === role).privileges : null
  
      userPrivileges = (
        [ 
          ...userPrivileges, 
          ...(group.privileges || []),
          ...(rolePrivileges || []),
        ]
        .map(e => ({ 
          ...e, 
          params: (
            Object.entries(e.params || {})
            .reduce((acc, [name, val]) => ({
              ...acc,
              [name]: paramMap[name] ? paramMap[name](val, group) : val   
            }), {})
          )
        }))
      )
    }
  }

  return mergePermissions(userPrivileges)
}

module.exports = getUserPermissions
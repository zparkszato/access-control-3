"use strict";

var permissionExistsForAction = function permissionExistsForAction(action, permissions) {
  var perm = permissions.find(function (e) {
    return e.name === action;
  });
  if (perm) return true;
  return false;
};

module.exports = permissionExistsForAction;
"use strict";

var permissionExistsForAction = require('./permissionExistsForAction');

var getUserPermissions = require('./getUserPermissions');

var canUserDoAtAll = function canUserDoAtAll(action, user, groups) {
  var perms = getUserPermissions(user, groups);
  return permissionExistsForAction(action, perms);
};

module.exports = canUserDoAtAll;
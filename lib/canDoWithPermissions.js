"use strict";

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var _require = require('./utils'),
    builtIns = _require.builtIns;

var validators = builtIns.paramValidators;

var canDoWithPermissions = function canDoWithPermissions(action, params, permissions) {
  var perm = permissions.find(function (e) {
    return e.name === action;
  });
  if (!perm) return false;
  return Object.entries(params).map(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        key = _ref2[0],
        val = _ref2[1];

    if (!validators[key]) {
      console.warn("Provided a param (\"".concat(key, "\") for which there is no validator"));
      return true;
    }

    return validators[key](perm.params, val);
  }).every(function (e) {
    return e;
  });
};

module.exports = canDoWithPermissions;
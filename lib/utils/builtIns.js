"use strict";

var utils = require('./utils');

var paramMap = {
  groups: function groups(param, group) {
    return param.map(function (p) {
      if (p === 'self') return group._id.toString();
      if (p === 'not-self') return "!".concat(group._id);
      if (p === 'any') return '*';
      return p.toString();
    });
  }
};
var paramValidators = {
  group: function group(_ref, value) {
    var groups = _ref.groups;
    if (groups.includes(utils.excludeForm(value))) return false;
    if (groups.includes('*')) return true;
    if (groups.find(function (e) {
      return utils.isExclude(e);
    })) return true;
    if (groups.includes(value)) return true;
    return false;
  }
};
module.exports = {
  paramMap: paramMap,
  paramValidators: paramValidators
};
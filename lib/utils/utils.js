"use strict";

var includeForm = function includeForm(str) {
  return str.toString().replace(/!/, '');
};

var excludeForm = function excludeForm(str) {
  return "!".concat(includeForm(str));
};

var isExclude = function isExclude(str) {
  return str.toString().match(/^!.*?$/) ? true : false;
};

var isInclude = function isInclude(str) {
  return !isExclude(str);
};

module.exports = {
  includeForm: includeForm,
  excludeForm: excludeForm,
  isExclude: isExclude,
  isInclude: isInclude
};
"use strict";

var canDoWithPermissions = require('./canDoWithPermissions');

var getUserPermissions = require('./getUserPermissions');

var canUserDo = function canUserDo(action, params, user, groups) {
  var perms = getUserPermissions(user, groups);
  return canDoWithPermissions(action, params, perms);
};

module.exports = canUserDo;
"use strict";

var getUserPermissions = require('./getUserPermissions');

var getUserPermissionsForAction = require('./getUserPermissionsForAction');

var canUserDo = require('./canUserDo');

var canDoWithPermissions = require('./canDoWithPermissions');

var permissionExistsForAction = require('./permissionExistsForAction');

var canUserDoAtAll = require('./canUserDoAtAll');

var utils = require('./utils');

module.exports = {
  getUserPermissions: getUserPermissions,
  getUserPermissionsForAction: getUserPermissionsForAction,
  canUserDo: canUserDo,
  canUserDoAtAll: canUserDoAtAll,
  canDoWithPermissions: canDoWithPermissions,
  permissionExistsForAction: permissionExistsForAction,
  utils: utils
};
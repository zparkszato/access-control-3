"use strict";

var getUserPermissions = require('./getUserPermissions');

var getUserPermissionsForAction = function getUserPermissionsForAction(action, user, groups) {
  return getUserPermissions(user, groups).find(function (e) {
    return e.name === action;
  });
};

module.exports = getUserPermissionsForAction;
"use strict";

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var merge = require('deepmerge');

var _require = require('./utils'),
    includeForm = _require.includeForm,
    excludeForm = _require.excludeForm,
    isExclude = _require.isExclude,
    isInclude = _require.isInclude,
    builtIns = _require.builtIns;

var paramMap = builtIns.paramMap;

var groupPermissions = function groupPermissions(permissions) {
  return permissions.reduce(function (acc, perm) {
    return _objectSpread({}, acc, _defineProperty({}, perm.name, [].concat(_toConsumableArray(acc[perm.name] || []), [{
      params: perm.params || {}
    }])));
  }, {});
};

var combineMerge = function combineMerge(target, source, options) {
  var destination = target.slice();
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    var _loop = function _loop() {
      var item = _step.value;

      if (!(isExclude(item) && destination.includes(includeForm(item)))) {
        if (isInclude(item) && destination.includes(excludeForm(item))) destination = destination.filter(function (e) {
          return e !== excludeForm(item);
        });
        if (!destination.includes(item)) destination.push(item);
      }
    };

    for (var _iterator = source[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      _loop();
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return destination;
};

var mergePermissions = function mergePermissions(permissions) {
  return Object.entries(groupPermissions(permissions)).map(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        name = _ref2[0],
        list = _ref2[1];

    return _objectSpread({
      name: name
    }, merge.all(list, {
      arrayMerge: combineMerge
    }));
  });
};

var getUserPermissions = function getUserPermissions(user) {
  var groups = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  var userPrivileges = [];
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    var _loop2 = function _loop2() {
      var group = _step2.value;
      var member = group.members.find(function (m) {
        return m.id.toString() === user._id.toString();
      });

      if (member) {
        var role = member ? member.role : null;
        var rolePrivileges = role ? group.roles.find(function (r) {
          return r.name === role;
        }).privileges : null;
        userPrivileges = [].concat(_toConsumableArray(userPrivileges), _toConsumableArray(group.privileges || []), _toConsumableArray(rolePrivileges || [])).map(function (e) {
          return _objectSpread({}, e, {
            params: Object.entries(e.params || {}).reduce(function (acc, _ref3) {
              var _ref4 = _slicedToArray(_ref3, 2),
                  name = _ref4[0],
                  val = _ref4[1];

              return _objectSpread({}, acc, _defineProperty({}, name, paramMap[name] ? paramMap[name](val, group) : val));
            }, {})
          });
        });
      }
    };

    for (var _iterator2 = groups[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      _loop2();
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
        _iterator2["return"]();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  return mergePermissions(userPrivileges);
};

module.exports = getUserPermissions;